\chapter{Algorithms and methods}
\label{chapter:methods}
In this chapter, we describe all methods and techniques that we used to solve the semi-supervised anomaly detection on time-series data. In the previous part, the most important concepts and approaches used for building the final algorithm were reviewed.
\section{Approach overview}
\textbf{The general approach} to solving the problem is built around the semi-supervised model that receives a labeled dataset from the Expert. The model combines it with unlabeled data from sensors and produces a classification label for a data record. 
\begin{itemize}
\item Each parameter is being treated separately. In a chain of consecutive periodic API calls the \textbf{data is pulled from the API}. Each data record represents 3 independent parameters (Temperature, $CO_2$, humidity), all records accumulated into 3 corresponding vectors of the length $L$.
\item Accumulated trinity vectors $V_{acc}$ are separately transformed with a \textbf{rolling window technique} [31] and normalized.
\item The \textbf{dissimilarity representation} is found with respect to all the data used in model training. 
\item We assume, priorly to start of training, the \textbf{labeled dataset T1 is provided} by a qualified analyst who sets labels for important data points (markers) which represent both anomalies and points belonging to the trivial flow.
\item The training data with known labels (denoted as T1) and a new data (T2) is being fed to the model for each parameter separately, where the clustering is taking place.
\item Output label for each point in T2 is produced. The new data and labels are appended to the $T2_{daily}$ dataset.
\item Points which labels correspond to anomalies are detected. 
For each parameter, outliers are processed separately, the majority vote principle based on the anomaly severity is applied to determine whether the assembled 3-parameters data point is considered as an outlier. 
\item The nightly iteration of the SSL-RPC training utilizes labeled data $T1$ and unlabeled dataset $T2_{daily}$ containing all data points collected during last 24 hours.
\item Points tagged as anomalies by the night run are reported to the Expert who reviews them. If the Expert corrects a label for any of such points, it is added to T1 as a labeled point.
 \end{itemize}
While the main data stream is being constantly processed, every night the model generates the assembled prediction outliers on the full set of the daily data. This is done in order to make the model evolving under changing circumstances basing on the labels and decisions made by the Expert.\\ \\
The generalized architecture of the approach is shown in Figure 4.1.
\begin{figure}[H]
\centering
\includegraphics[width=1\linewidth]{./thesis_general}
\caption[Figure X.]{The clustering workflow at the full scale -- from getting the data to updating the model}
\label{fig:sensor4}
\end{figure}
\textbf{The implementation.} For implementing the described approach, Python programming language was selected. The selection was made basing on its functionality and features making this language one of the standard choices for data processing and analysis tasks. Also, it has a broad selection of libraries suitable for Machine Learning. The valid training data was stored in a Mongo DB instance. For retrieving the records from the API we used \textbf{json} library of Python. Mathematical operations were supported by \textbf{NumPy} and \textbf{SciPy} [30]. The DB access is organized with using of \textbf{SQLAlchemy}.
\section{Data preprocessing}
\textbf{Data retrieving} is being done in small batches occurring regularly within the predefined interval of time. In the project scope, we are mainly interested in 3 main environmental parameters -- temperature, $CO_2$ concentration and humidity which are queried by API via a JSON request, i.e.:\\
\textit{http://121.78.237.162:8000/otakaari4?start=2014/08/25-01:00:00\&end=2014/08/29-01:00:00\&number=4201\&type=co2\&token=aaltootakaari4}\\\\
The following request gets all CO2 measurements for room 4201 between 01:00 AM 25 Aug 2014 and 01:00 AM 29 Aug 2014. \\
Output example of the executed API command: \\ \\
\textit{
vtt\_otaakari4\_co2\_4201 1408943161 3555 host=keti3 nodeid=4201 vtt\_otaakari4\_co2\_4201 1408943445 3555 host=keti3 nodeid=4201 ... } \\

where "3555" is current CO2 value, and 10-digits number (i.e. 1408943161) is a timestamp of the measurement recorded by a sensor \\
We parse each line to extract the information from data:
\begin{itemize}
\item parameter name ($CO_{2}$ concentration, temperature or humidity)
\item room number
\item value
\end{itemize}
The API was designed by KETI specialists to generate a data point only in the case when a parameter value was changed.\\
\textbf{Regular sampling} is required as the data preparation step in order to normalize points distribution over the timeline. We transform the data to achieve a stable frequency $\Theta$. In our case $\Theta=30 \ sec$ is the interval between neighbor data points. Therefore, one minute of observation corresponds to 3 data records -- for 0 second, 30-th second and 60-th second. If there was no change, the previous value gets replicated to the next point which is 30 seconds further in time. In this setting, we transform time-series to the regularly sampled form as we aim at reducing amounts of information that have to be stored alongside with the main observation data. Another positive outcome of applying regular sampling to the data is avoiding values that sensor can produce when a measured parameter is placed on the edge of two consecutive minimal observation values which causes API to start reporting a parameter change on each transition between these values. This situation may lead to generating hundreds of almost equal measurements registered by a sensor and affecting the model learning set.\\
The selection $\Theta=30 \ sec$ as the sampling frequency parameter was done naively with respect to the expected use cases of the system which do not require more frequent updates of parameter values. Also, this very basic technique allows to make the data trend smoother and avoiding raising an alarm on instant outliers that are rarely spotted on the trend in the long run.\\  \\
\textbf{Forming input data vectors}. The approach to utilizing time-series data is based on the aims and goals of the thesis. As it was previously stated, the data classification in the framework occurs in two stages. The first stage, "fast classification" is done as soon as possible in order to verify whether or not is the recently arrived label an outlier. The second stage, "nightly classification" operates with all the data gathered within 24 hours.\\
Our approach to the data representation task is based on the idea of decomposing a time-dependent stream data of an infinite length into small fractions of the maximal length $l_{max}$ which are treated as a single $l_{max}$-dimensional data record. It allows us to use the proposed SSL-RPC classifier operating with clusters of independent time-series of maximal length  $l_{max}$.\\
Depending on the records sampling frequency $\Theta$, the data vector $L$ contains as many data points as it is required by the limit response time $T$ parameter. If $T = 0$, the model must be working in a real-time, however, it is not the exact use case for the project's model. Minimal model-compatible value $T = \Theta*S_{r}$, where $S_{r}$ is the size of the rolling window (see the next point). \\ \\
\textbf{Rolling window technique} [31] is applied to data points collected in $L$ before submitting it to the model input as T2. This is a basic technique in time-series analysis that allows to make peaks smoother and, therefore, avoid false positive outlier detection. Using the rolling window technique, the time-series information about preceding and succeeding data points is recorded in order to form an independent time-series mentioned in the previous item.\\
Let $\{y_{i}\}$ with $i = 1,...,N$ be a sampled time-series data of the length $N$. Then $W$ is an integer defining a size of the window such that $0 < W \leq N$ and $\forall k$ indexes of data points in $\{y\}$ where $W \leq k \leq N $. Then a new corresponding data point after applying window is $y^{'}_{k} = \{y_{k-W},y_{k-W+1},...,y_{k}\}$.\\ The window size $W=3$ was selected. The decision was made according to requirements of space, performance and data visualization possibility.\\ \\
\textbf{Normalization} is taking a place at the next step of the input data preprocessing and aims at meeting the requirements for the input data  that is fed to the Stochastic Gradient Descent outlined by LeCun et al. in [32]. Researchers suggest to remove an average of each of the input variables and normalize their variances. Therefore, for each parameter we subtracted the parameter mean $\mu$ and divided the result by the standard deviation $\sigma$:
$$x^{'} = \frac{x \ - \ \mu }{\sigma}$$
The resulting data is known as such that grants better gradient descent performance and convergence rates [32].
\\
The resulting data is transformed to the form of the \textbf{pairwise dissimilarity} using one of existing valid distance functions -- in our case, Euclidean distance [8]. The Euclidean measure was selected as one recommended by RPC method authors [2,6]. However, they mention that non-Euclidean distance functions can also be applicable to the classifier which opens a space for performing a research which is being out of the scope of this project.
\section{Model and training}
The model for clustering data contains two main modules -- the RPC classifier [2] and the Conformal Prediction algorithm [6].\\ \\
\textbf{RPC classfier} is based on the approach described in the 2. It operates with the dataset denoted as $T1$ -- the labeled dataset which serves as the training base for the RPC. The algorithm updates the prototypes positions in order to fit them to the current data and achieve the most accurate representation in terms of the cost function minimization.\\ 
RPC optimization is done using the Stochastic Gradient Descent method. In our implementation the original RPC update rules were transformed to the following form:

\begin{equation}
\Delta \gamma^{+}_{k} = - \Phi^{'}(\mu(v_{i})) \cdot \mu^{+}(v_{i}) \cdot \frac{\partial ([D \gamma^{+}]_{i} - \frac{1}{2} \cdot (\gamma^{+})^{t}D\gamma^{+})}{\partial \gamma^{+}_{k}} \cdot \frac{1}{\lambda}
\end{equation}
\begin{equation}
\Delta \gamma^{-}_{k} = - \Phi^{'}(\mu(v_{i})) \cdot \mu^{-}(v_{i}) \cdot \frac{\partial ([D \gamma^{-}]_{i} - \frac{1}{2} \cdot (\gamma^{-})^{t}D\gamma^{-})}{\partial \gamma^{-}_{k}} \cdot \frac{1}{\lambda}
\end{equation}
where $0<\frac{1}{\lambda}<1$ is a regularization coefficient introduced in order to achieve a better convergence of the update steps. As we found out when performing visualized experiments, the original update rules lead to gradient descent steps which miss the global minimum areas and never converge. We applied a basic regularization technique to the gradient descent steps and reported a change of model performance in the next section. The exact values of $\lambda$ might depend on the data nature and need to be investigated. Further, we perform empirical research on the model behavior under different $\lambda$ values in order to find optimal limits for solving the project's task. \\

\textbf{Semi-supervised Learning using RPC: Conformal prediction} \\In the chapter 2.8 we sketched the Semi-supervised extension of RPC. It is based on forming a low-credibility region $\Gamma$ and examining points from the unlabeled dataset $T2$ through $credibility$ and $confidence$ parameters to find out whether the point belongs to $\Gamma$ or not. The practical application of the method includes some additional comments and assumptions.\\
The key feature of the SSL-RPC approach is the automatic adaptation of the model complexity and  the number of prototypes used for representing the data. Initially, we use labeled set T1 for the preliminary model training. T2 is used for verifying the quality of clustering after each iteration of the full RPC classifer update. Firstly, we compute $credibility$ and $confidence$ for each data point in T1 to form the set $\bm{\beta}$ corresponding to the $\Gamma$ region from the chapter 2.8. To recall, the low confidence for the point from $T2$ is defined as $(1 - \epsilon^{i}_{1}) \leq (1 - \frac{1}{L})$, low credibility: $\epsilon^{i}_{2} \leq \frac{1}{L}$, where $L$ is the length of the dataset referred to as $T1$. In [2] researchers define the universal rule for adding points to $\bm{\beta}$:

\begin{equation}
\bm{\beta} = \big\{v_{i} \in T2 \ : \ (1 - \epsilon^{i}_{1}) \leq (1 - \frac{1}{L}) \ \vee \ \epsilon^{i}_{2} \leq \frac{1}{L} \big\}
\end{equation}
The size of the $\bm{\beta}$ region also works as stopping criterion for the prototype fitting algorithm. The researchers emphasize that the size is not supposed to be equal to $0$ in order to confirm a good fit of the clustering. Vice versa, empty $\bm{\beta}$ indicates too dense clustering [2]. In the experiments on the simulated data it was found that optimal size of $\bm{\beta}$ is $\theta = 5$ points, therefore the convergence criterion for the algorithm we use is $\theta : |\bm{\beta}| \leq 5$

\subsection{Classification algorithm: RPC + SSL}
The Semi-Supervised RPC training algorithm can be represented in the pseudocode (see Alg.\ 1).

\begin{algorithm}[H]
\begin{algorithmic}[1]
\State $Beta := \emptyset$
\State \textbf{Initialize prototypes} W
\State $W \gets \ $ \textbf{TrainRPC} on $T1$ from current \textit{W}
\State $Wbest \gets W$
\State $NewLabels \gets \ $\textbf{Conformal prediction} $T2$ with prototypes $W$
\State $NewLabelsBest \ \gets\ NewLabels$
\State $Beta \gets \ $\textbf{points} from the region of uncertainty in T2
\While{$|Beta| \geq \theta \text{ \textbf{or} \textit{max-iter-count}} \leq 50$}
\For {each \textbf{cluster} within $Beta$}
    \State $Beta\_unc \gets \ $\textbf{generate new prototype vector}(s) from Beta
    \State $W \gets W \ \cup \ Beta\_unc $
\EndFor
\State $W \gets \ $ \textbf{TrainRPC} on $T1$ from current \textit{W}
\State $NewLabels \gets \ $\textbf{Conformal prediction} $T2$ with prototypes $W$
\State $S\_index \gets calculate \ \textbf{Silhouette index} \ for \ NewLabels$
\If {$S\_index\geq S\_index\_best$}
    \State $NewLabelsBest \ \gets\ NewLabels$
    \State $Wbest \gets W$
    \State $S\_index\_best \gets S\_index$
\EndIf
\EndWhile
\State \textbf{Return} $Wbest,\ NewLabelsBest$
 \caption{The model main algorithm: Semi-supervised RPC}
\end{algorithmic}
\end{algorithm}
This algorithm unites the principles and approaches explained in the previous chapters. After the first run of the RPC classifier and Conformal prediction (steps 1-6), the model forms the first set of labels denoted as $NewLabels$ for T2 and the first iteration of the prototypes $W$, which initial values were updated in order to achieve a better data fit.\\
From this point, the model starts fitting prototypes to the data using the set $\bm{\beta}$ consisting of T2 points with low confidence/credibility (step 6). In this loop, the model is trying to create new prototypes from $\bm{\beta}$ fitting to low-confidence data. It leads to more accurate classification results for unlabeled set T2. The data fit is measured by the Silhouette index [33] -- the prototype configuration fitting both T1 and T2 the best is stored. \\ 
As the loop exit criterion, the size of the region $\bm{\beta}$ is used. When it falls below the threshold $\Theta$, the model reached its training limit with current data, and there is no feasibility in creating new prototypes from $\bm{\beta}$. Another possible exit criterion is the fixed number of iterations. We use \textit{max-iter-count} $\leq 50$ which can be tuned according to the model needs and time restrictions. \\
It has to be kept in mind that each iteration performs Conformal Prediction with creating new prototypes from the low-confidence data. Therefore, if after N iterations the size of $\bm{\beta}$ is not decreasing it might be an indicator of inseparable clusters. Also, each new iteration of the loop increases computational load as more and more prototypes are spawned to the data space.

\subsection{RPC algorithm in details}
The general RPC training procedure is very similar to one described in the literature [2,6] and in the previous chapters. Its pseudocode used in the thesis implementation is referred to as Algorithm 2:

\begin{algorithm}[H]
\begin{algorithmic}[1]
\State \text{Given } $\bf{D, W}$ \Comment{ \parbox[t]{0.4\linewidth}{D -- T1 pairwise distances matrix \\W -- initial prototype}}
\State $maxiter \gets 50$
\While{ $iter < maxiter$}
\For {each row in $\bf{D}$} \Comment{ \parbox[t]{0.4\linewidth}{each row represents all dissimilarities of one point in T1}}
    \State $\gamma^{+} \gets \ $\text{the closest prototype with the same label}
    \State $\gamma^{-} \gets \ $\text{the closest prototype with a different label}    
    \State \text{Find} $\nabla(\gamma^{+})$ 
    \State \text{Find} $\nabla(\gamma^{-})$ 
    \State $W(\gamma^{+}) \gets W(\gamma^{+}) + \nabla(\gamma^{+}) \cdot \lambda$ \Comment{\parbox[t]{0.4\linewidth}{Update corresponding to $\gamma^{+}$ prototype vector}}
    \State $W(\gamma^{-}) \gets W(\gamma^{-}) + \nabla(\gamma^{-}) \cdot \lambda$ \Comment{\parbox[t]{0.4\linewidth}{Update corresponding to $\gamma^{-}$ prototype vector}}
    
\EndFor
\State Calculate the Cost function $E$
\If {$E < E\_best$}
    \State $E\_best \gets E$
    \State $W\_best \gets W$
\Else
    \If{$E - E\_best > \rho$} \Comment\parbox[t]{0.4\linewidth}{{$\rho$ is a maximal allowed convergence threshold}}
        \State $W \gets W\_best$
        \State \textbf{break}
    \EndIf
\EndIf
\EndWhile
\State \textbf{Return} $W$
 \caption{Relational prototype classification step (RPC) in details}
\end{algorithmic}
\end{algorithm}
The algorithm implements the RPC classifier described in previous chapters, and practically performs a limited number of full stochastic gradient descent iterations, updating prototypes in $W$ in order to achieve the best possible fit to the data T1 represented in the form of dissimilarities. The convergence is reported when the cost function $E$ starts to diverge and does not improve the overall result fit anymore. \\
In [2], the RPC convergence criterion is proposed as $E - E_{best} > 0$, which means, the algorithm breaks at the first iteration when the cost function starts growing. However, according to [1,6,7], one of the LVQ algorithm drawbacks is the fact that it can possibly get stuck in the local minima after updating the prototypes. We assumed if we use some small positive threshold $\rho$ instead of \textbf{zero}, the model can continue descent after reaching the local minima. Therefore, we set the stopping criterion as $E - E_{best} > \rho$, where $0 < \rho < 1$.
\subsection{New prototype creation algorithm}
On the each iteration after forming the set $\bm{\beta}$, we are interested in improving the clustering fit by creating a new prototype fitting $\bm{\beta}$ and decreasing its size. The researchers in [2] do not provide sufficient details on how to generate a new prototype. Therefore, this procedure was developed and tested within out project (steps 9-12 of the Algorithm 1).\\
First of all, we know from [2] that the new prototype "is set to the representative data point (median) in $\bm{\beta}$". It is labeled with the same label as the nearest neighbor from $T1$.\\
The general procedure of getting new prototypes out from $\bm{\beta}$ and adding them to the main prototype set $\mathbf{\gamma}$ used for training and fitting the model is described in the pseudocode Algorithm 3:
\begin{algorithm}[H]
\begin{algorithmic}[1]
\State $\bm{\beta} \gets \text{T2 points of low confidence / credibility}$
\State \textbf{Find NN-clusters within} $\bm{\beta}$
\For{\text{each cluster} $\bm{\beta i}$ \text{in} $\bm{\beta}$}
    \For {\text{each coordinate of data points in} $\bm{\beta i}$}
        \State \text{Find the median point position}
    \EndFor
    \State \text{Form} $S_{r}$ \text{coordinate-wise binary prototype vectors} $\gamma_{\bm{\beta_{i}}^{n}}$
    \For {\text{each prototype vector in} $W$} 
        \State \text{Extend} $W$ \text{with} $|\gamma_{\bm{\beta_{i}}}^{n}|$\text{-zeros}: $W \gets W \cup [0,..,0]$  
    \EndFor    
    \For {\text{each prototype vector} $\gamma_{\bm{\beta_{i}}}^{n}$} 
        \State \text{Assign the label of the} $\bm{\beta_{{i}_{median}}}$ \text{'s NN from T1}
        \State \text{Extend} $\gamma_{\bm{\beta_{i}}}^{n}$ \text{with} $|T1|$\text{-zeros}: $\gamma_{\bm{\beta_{i}}}^{n} \gets [0,..,0] \cup \gamma_{\bm{\beta_{i}}}^{n}$
        \State Append $\gamma_{\bm{\beta_{i}}}^{n}$ \text{to the main prototype set} $W:$ $W \gets W \cup \gamma_{\bm{\beta_{i}}}^{n}$
    \EndFor
    \State \text{Append points from} $\bm{\beta_{i}}$ \text{to T1} 
\EndFor
 \caption{New prototypes handling procedure}
\end{algorithmic}
\end{algorithm}
\begin{enumerate}
\item The preliminary step is to determine how many prototypes does $\bm{\beta}$ contain (step 2 of Alg.\ 3). We aim at minimizing the number of prototypes to avoid overfitting the model, so the least number possible is equal to the number of unique clusters within $\bm{\beta}$. In order to solve the clustering task and minimize the computational complexity we can use existing prototypes positions. The algorithm finds the nearest neighbor of each point $\bm{\beta}$ in the set formed of points that represent the corresponding prototype cluster in space. Basing on the definition [2] of $W$, we can calculate the position of $i$-th prototype as a dot product of each of prototypes and the data in T1:
$$pos_{i} = W_{i} \cdot T1$$
The vector $P_{pos}$ of prototype positions is formed and labels of corresponding prototypes are assigned to their positions in $P_{pos}$.\\
Finally, the nearest neighbors for each point in $\bm{\beta}$ are found from $P_{pos}$ and the corresponding label is assigned to the point in $\bm{\beta}$. The final number of clusters in $\bm{\beta}$ denoted as $i$, is equal to the number of unique labels of $\bm{\beta}$-points. Therefore, each of the clusters is denoted as $\bm{\beta}_{i}$. \\All the following steps (2-6) are performed for each of $\bm{\beta}_{i}$ clusters separately.
\item Find the median point in $\bm{\beta}_{i}$.\\ 
Assume, we have a list of data records denoted as $\bm{\beta}_{i}$. Finding the median in the case when each single point in $\bm{\beta}_{i}$ is having a data record of length \textit{one} is an ordinary task. Nevertheless, in our case, each record is represented as a  $1 \times S_{r}$ data vector, where $S_{r}$ corresponds to the length of rolling window. The question is how to find a median for $1 \times S_{r}$-dimensional data record, and then use this information for creating a prototype having dimensions $1 \times N$, where $N$ stands for the length of $\bm{\beta}_{i}$.\\
The possible solution is to create a new prototype for all of $S_{r}$ coordinates of all $\bm{\beta}$ points, assigning it to a corresponding point from the vector containing  $S_{r}$-th coordinate's records of each of $N$ points from $\bm{\beta}_{i}$: $$[\bm{\beta}_{i}^{1} = \bm{\beta}_{i_{1}}^{1},\bm{\beta}_{i_{2}}^{1},\bm{\beta}_{i_{3}}^{1},..,\bm{\beta}_{i_{N}}^{1}]$$
$$[\bm{\beta}_{i}^{1} = \bm{\beta}_{i_{1}}^{2},\bm{\beta}_{i_{2}}^{2},\bm{\beta}_{i_{3}}^{2},..,\bm{\beta}_{i_{N}}^{2}]$$
$$...$$
$$[\bm{\beta}_{i}^{S_{r}} = \bm{\beta}_{i_{1}}^{S_{r}},\bm{\beta}_{i_{2}}^{S_{r}},\bm{\beta}_{i_{3}}^{S_{r}},..,\bm{\beta}_{i_{N}}^{S_{r}}]$$
Now the task is to find a median point in each of $S_{r}$-$\bm{\beta}_{i}^{S_{r}}$ vectors. According to the median definition, we sort all numbers in ascending orders and store the index of the one in the middle, if the number of elements is even. Otherwise, we examine two elements placed in the center and check which of them is closer to the real median of $\bm{\beta}_{i}$ computed by the NumPy.median function.
\item Create new prototypes $\gamma_{\bm{\beta}_{i}}$ representing the data points from $\bm{\beta}_{i}$\\
At this step we need to find a prototype fitting to points from $\bm{\beta}_{i}$. Previously, we found the indexes of median data points in each of $\bm{\beta}_{i}^{S_{r}}$ coordinate vectors, where $S_{r}$ is the length of the rolling window. Therefore, we can create $S_{r}$ prototype vectors each of them can be represented as a vector of the length $(|\bm{\beta}_{i}^{S_{r}}| - 1)$-\textbf{zeros} and the digit \textbf{\textit{1}} placed in the position of the index corresponding to the index of  the median point in $\bm{\beta}_{i}^{S_{r}}$ for each $S_{r}$ selected for representing it, i.e.: $\gamma_{\bm{\beta}_{i}^{S_{r}}}=[0,0,..,0,1,0,..,0,0]$. 
This is done in order to assign the prototype to the median point and make sure that the requirement to a prototype $\sum_{k} \gamma_{\bm{\beta}_{i_{k}}^{S_{r}}} = 1$, where $\gamma_{\bm{\beta}_{i_{k}}^{S_{r}}}$ is the $k$-th element of the prototype vector $\gamma_{\beta_{i}^{n}}$, is satisfied.\\
As the outcome of this step, we form the set of $S_{r}$ binary vectors $\gamma_{\bm{\beta}_{i}^{S_{r}}}$, and the data record for the median point which contains median points of each of $S_{r}$ dimensions of data records in $\bm{\beta}_{i}$: 
$$\bm{\beta}_{i_{median}} = (\bm{\beta}^{1}_{i_{median}},\bm{\beta}^{2}_{i_{median}}, .. ,\bm{\beta}^{S_{r}}_{i_{median}})$$
\item Assign the label to $S_{r}$ new prototypes $\gamma_{\beta_{i}^{S_{r}}}$ and points in $\bm{\beta}_{i}$.\\
This label corresponds to the $\bm{\beta}_{i_{median}}$ nearest neighbor point's label from $T1$, denoted as $\bm{l_{\gamma_{\beta_{i}}}}$. All points in current $\bm{\beta}_{i}$ will receive this value.\\
All newly created prototypes $\gamma_{\bm{\beta}_{i}^{S_{r}}}$ also receive the same label $\bm{l_{\gamma_{\beta_{i}}}}$
\item Extend existing prototype vectors $W$\\
Assume, we recently added $N$ points to T1 and appended new prototype vectors $\gamma_{\bm{\beta}_{i}^{S_{r}}}$. However, any of previously existing $k$ prototype vectors denoted as $W^{k}$ has dimensions $1 \times |T1|$, while the new $\gamma_{\bm{\beta}_{i}^{S_{r}}}$ are as large as $1 \times|T1| + |\bm{\beta}_{i}|$.\\
Since existing prototypes $W$ are the linear combination of the all T1 points, we need to extend each of old prototype vectors $W^{i}$ by $N = |\bm{\beta}_{i}|$ records. To satisfy the requirement  $\sum_{k} w^{i}_{k} = 1$ and not break current prototype-data fit, we extend each $W^{i}$ vector with $N$ zeros appended to the end of the vector.
\item Add the new prototype formed from $\bm{\beta}_{i}$ (step 2) to $W$.\\
Each of $S_{r}$ new prototypes $\gamma_{\bm{\beta}_{i}^{S_{r}}}$ needs to be appended to $W$. The horizontal dimension of $W$ is equal to $|T1| + |\bm{\beta}_{i}|$, while the length of $\gamma_{\bm{\beta}_{i}^{S_{r}}} = |\bm{\beta}_{i}|$. To make appending possible, we need to extend $\gamma_{\bm{\beta}_{i}^{S_{r}}}$ with $|T1|$-zeros from the left, and after that to append the new prototype to $W$.
\item Append points from $\bm{\beta}_{i}$ to T1.\\
This step is required since at this stage the model already determined the labels for the points and now is aimed at finding the best prototype representation of T1 $\vee$ T2. In the next iteration some of the points added in this step may again appear in $\bm{\beta}_{i}$ and potentially get different label. We assume $N$ points are added to the end of the data vector T1.\\Their labels are added to the vector $Y$ containing label assignments for T1 data points. It is important to note that points from T2 are labeled and added to T1 temporarily, within the current iteration scope.
\end{enumerate}

For example, the prototype matrix $W$ after performing the step 7 might have the following look:

$$W = \bigg[\begin{tabular}{cccccc}
 0.1 & 0.2 & 0.5 & 0.2 & 0 & 0 \\ 
 0.7 & 0.1 & 0.1 & 0.1 & 0 & 0 \\ 
0 & 0 & 0 & 0 & 1 & 0 \\
0 & 0 & 0 & 0 & 0 & 1 \\   
\end{tabular}  \bigg]$$
where $W^{3}, W^{4}$ are the newly added prototype vectors, the length $|\beta| = 2$, and as the median point corresponds to $(\beta^{1}_{1},\beta^{2}_{2})$.

\subsection{Initialization of prototypes}
Initialization of prototypes is proposed to perform as a random procedure [1,2,6]. However, we noted that the developed SSL algorithm for RPC classifier is relying on initial assumptions about prototypes. On the steps 5 and 14 we perform so-called \textbf{Conformal prediction}, which forms the region $\beta$ of points with uncertain assignments. In the case, the initial labels are wrong (i.e. randomly initialized prototypes were placed quite close in space with different labels assigned) the algorithm's performance might be affected by fitting the initially wrong prototype positions to data.\\
To improve the initial prototype knowledge, we decided to experiment with a semi-random or warm-start initialization. We create $N$ prototypes as vectors of the length $l = |T1|$ composed of $(l-1)$-zeros and the digit \textbf{1} in the position of the corresponding point. \\ 
The warm-start still allows the algorithm to correct position of prototypes in further iterations relying on more accurate initial labeling.\\ \\
\textbf{Number of prototypes on the initialization} \\
Since we experiment with the warm-start prototype initialization, a number of prototypes on the start might affect the accuracy of the model. It is a relatively cheap procedure, as in the each next iteration we will generate at least as many as $S_{r}$-prototypes out of $\bm{\beta}$. Therefore, it seems more beneficial to provide prototypes set better fitting the data in T1 from the initialization step.\\
The prototype generation techniques can depend on:
\begin{itemize}
\item  data distribution -- create more prototypes for dense areas in dataset
\item initial number of clusters -- given that we know the initial number of classes and relative distribution points over them. In this case, it is possible to create more prototypes for larger clusters in order to represent them more precisely
\item employ information extracted from previous runs -- as we always keep the recent history of measurements and assigned labels, it is also possible to automatically tune number of prototypes basing on the data of past executions
\end{itemize}
\section{Algorithm computational complexity}
The complexity of the Semi-Supervised Relational Prototype Classification is proven [2] to scale quadratically in complexity $O(N^{2})$ in the number of training examples and linearly $O(N)$ in sizes of prototypes.\\
At the same time kernel approaches such as RBF scale as $O(N^{3})$, as well as non-enhanced $S^{3}VM$ -- Semi-Supervised Support Vector Machines [40]. Which makes the SSL-RPC a reasonably good alternative in terms of running times and efficiency of computations.
\section{Anomaly detection}
Anomaly detection is based on the output generated by the SSL-RPC model. This procedure occurs in two stages -- firstly the model detects outliers on each data vector $L$ received for the input. The size of $L$ is relatively small, predictions generated are local, light-weight in terms of computational power. However, they do not correspond to the whole picture. The second iteration happens every night when the daily data is analyzed, outliers are reported to the analyst who applies corrections to T1 if it is necessary.\\
The anomaly detection model has to be able to recognize 5 general clusters of data and produce numeric labels for parameters $l_{temp}, \ l_{CO_2}, \ l_{hum} \in (1,2,3,4,5)$. We propose the following classification:
\begin{enumerate}
\item Significant anomaly under the expected norm. This cluster represents the anomalies carrying a strong evidence of a deviation from the main data flow. In the target system perspective, this category represents the values that are definitely being anomalous and must be reported straight after finding them.
\item Medium anomaly under the expected norm. The cluster incorporates values that lay under the expected norm, however, still can be classified as non-harmful to neither a human health or the room's equipment. This is the transitional class which might show either a subnormal behavior of the system or be just a deviation from the norm that does not need to be reported as an anomaly.
\item Expected norm. Represents the widest cluster within the data. Most of the records belong to this category and are handled as a non-anomalous data with the highest level confidence.
\item Medium anomaly above the expected norm. Has the same severity as the Medium anomaly under the expected norm.
\item Significant anomaly above the expected norm. Has the same severity as the Significant anomaly under the expected norm.
\end{enumerate}
This general classification is applied "as-is" to the \textbf{Temperature data} which is required to be within the corridor of the comfortable environment. Significant deviation from it may lead to too cold or too hot rooms.\\
In the case of measuring $\bm{CO_{2}}$, the classes denoted as 1 and 2 are eliminated from the process. $\bm{CO_{2}}$ concentrations being under the average are not affecting human performance in any way.\\
\textbf{Humidity} parameter follows the same anomaly detection rules as the \textbf{Temperature}. Either very humid or dry conditions may lead to establishing a non-comfort and unacceptable environment inside the building rooms.\\
The following classification rules were established to detect whether the data is anomalous with \textbf{high confidence} (corresponding to classes 1 and 5 from the SLL-RPC output), \textbf{low confidence} (classes 2,4) or normal (class 3).\\ \\
\textbf{Multi-parameter anomaly detection}\\
As stated before, the developed model operates with 3 different environment parameters -- temperature, CO$_2$ and humidity. The outputs $l_{temp}, \ l_{co2}, \ l_{hum}$ of each parameter need to be combined with others. The resulting binary label $l_{final} \in (0,1)$ triggers the system anomaly signal and sends the data to the ventilation controlling systems which can perform an action in order to improve the situation (the corrective action system development is beyond the current project's topic).\\
The label $l_{final} \in (0,1)$ is generated by the anomaly detection procedure based on the confidence determined for each of parameters in the previous steps. Simple rules were taken in use:
\begin{itemize}
\item If there was detected any of high confidence labels: $l_{temp | hum} \in (1,5)$ or $l_{CO_2} = 5$, the system anomaly is detected: $l_{final} \gets 1$
\item If there was detected one or more of high confidence labels: $l_{temp | hum} \in (2,4)$ or $l_{CO_2} = 4$, the majority vote principle is applied to the labels $l_{temp}, \ l_{co2}, \ l_{hum}$. Two or more low confident labels lead to $l_{final} \gets 1$. If there is only one low confident label and two other are being within the normal limits, $l_{final} \gets 0$
\item If all labels are located within normal limits $l_{temp}, \ l_{CO_2}, \ l_{hum} = 3$, then $l_{final} \gets 0$
\end{itemize} 
All low confidence reported during the daily execution can also be logged in the special report that can be further analyzed.\\ \\

\textbf{Human teacher supervision of the model}\\
The model is constantly supervised by an analyst who checks the results of the anomaly detection and can edit labels generated for data points. During this procedure the Expert modifies an entry in the Main Database and applies the human set label to it. Improvements made by the human allow making classification results more solid and reliable.
\section{Storing data and updating it from newest observation}
The dataset denoted as T1 is stored in the Main Database in the form of:
\begin{itemize}
\item Observation date and time (in the format of dd-mm-yyyy)
\item Parameter name
\item Value
\item Label
\end{itemize}
We developed the ensemble technique for keeping the dataset of reasonable size and ensuring that new observations are handled with more importance than old ones.\\  
Therefore, made of sliding window approach [41] united with the modified version of the forgetting factor approach [42]. A sliding window of size $T$ corresponds to a constant interval of time for which we keep the records in the DB. It can be defined in values of hours, days or months depending on the system needs. All values which are older than $T$ are dropped.\\