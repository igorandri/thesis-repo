\chapter{Experiments and results}
\label{chapter:implementation}
The goal of this section is to represent the achieved results and describe the experiments performed in model training and anomaly detection.\\
We aim at providing a broad overview of the model nature since, in our opinion, it was not widely explored in a known literature.  Therefore, we use three main dataset classes in experiments. The first one named the synthetic or toy data representing a number of known artificial datasets (such as Gaussians) aimed to better demonstrate generalization and prediction properties of the model. The third class unites the datasets often used by researchers who develop SSL algorithms. We will use UCI datasets to compare results produced by our model to the original RPC results. The third class represents a real data received from the sensors over the time. This is the main data that the project is applied to.
\section{Experiments with synthetic data}
\subsection{T1/T2 experiment}
This experiment plays the core role in the verifying RPC-SSL model's eligibility to be applied to the project's specific data and its processing pipeline. According to the project goal definition, the main requirement to SSL-RPC is to be able to handle unlabeled data set having significantly larger length comparing to labeled, and otherwise.\\
The first case is related to most of the known semi-supervised learning setups and is mostly related to the Model Nightly run, the second case corresponds to a real-time run.\\
As we previously stated, the back-end handles the data when it arrives, accumulates it before sending to the model for clustering. The length of the data vector $L$ being sent to the model is definitely short since observations arrive consequently with some frequency $\Theta$. For example, the vector of length $|L| = 1..10$ would contain predictions generated every 0.5 -- 6 minutes in the current project model setting. \\
At the same time, authors in [2] clearly state that the labeled dataset T1 is used for training the model. The conformal prediction part including generating a new label for an arrived data point is performed over  the unlabeled dataset T2. The Conformal Prediction procedure also includes the step of fitting the model to new labels and generating prototypes basing on new data points. In the author's setting, length $|T2|$ was dominating over $|T1|$. The default proportion of unlabeled data to labeled  has been introduced as 3:1 or 4:1. However, researchers do not comment the motivation behind the selection of this proportion and how the model behaves in other scenarios.\\
In our daily iteration setting, the proportion of lengths T2:T1 is the opposite, as we aim at delivering real-time or close to real-time results in the first phase of prediction (the Real-time run), and we cannot afford to accumulate the dataset T2 to follow the proportion proposed in [2]. Therefore, before we can proceed with main experiments, we must discover the model's behavior on the opposite proportion, when T2 is smaller than T1, and verify that the model is generally suitable for using with this kind of the dataset.\\
To answer this question we constructed two types of randomized datasets:
\begin{itemize}
\item \textbf{G-1}. Randomized toy dataset of two isotopic Gaussians blobs with binary class labels $y = {0,1}$, Standard deviation $\Sigma = 1.0$, and blobs centers located at (3,3) and (0,0). This dataset represents two clusters of data which are located close to each other and in most of the cases could not be separated linearly, see Fig.\ 5.1.
\item \textbf{G-2}. Randomized toy dataset of two isotopic Gaussians blobs with binary class labels $y = {0,1}$, standard deviation $\Sigma = 1.0$, and blobs centers located at (2,2) and (0,0). This dataset represents two clusters of data which contain areas where points of opposite classes overlap, see Fig.\ 5.2.
\end{itemize}
The intuition behind the selection of this two types of data is based on known characteristics of RPC [6] and RPC-SSL [2]. We assume if the proportion $T2:T1 >= 3:1$ is essential in training the model, then the accuracy of the opposite proportion must be generally worse than for the original. The second reason for using both \textbf{G-1} and \textbf{G-2} is that in the case of the hardly separable dataset \textbf{G-2} the most of the important information for clustering is extracted on the Conformal Prediction step involving and depending on T2.\\
We generated datasets $\text{G-1}$ and $\text{G-2}$, where the size of $\text{G-1}$ is static $|\text{G-1}| = 50$, while the size $|\text{G-2}|$ is changing in every iteration of clustering basing on the proportion T2:T1 
$$|\text{G-2}| = (|T1|*0.1,|T1|*0.15,,|T1|*0.2...,|T1|*P)$$ 
where $P=3.0$.\\
The RPC-SSL model's initial prototype selection technique was set to the "point from the data", regularization coefficient $\lambda$ for RPC training was set to $\lambda = 1/1000$, maximal number of RPC descent steps -- 10.\\
\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{./clustt1t2.png}
\caption[Figure X]{Random initialization of G-1 data with partially overlapping blobs with centers (3,3),(0,0)}
\label{fig:window}
\end{figure}
Figure 5.1 contains an example of the G-1 initialization. The data T1 is plotted with round dots, where the dot color corresponds to the cluster label. The stars denote the dataset T2 and carry the same color coding information.
\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{./t1t2_22_00_centers.png}
\caption[Figure X]{Random initialization for G-2 with overlapping blobs with centers (2,2), (0,0)}
\label{fig:window}
\end{figure}
The goal is to train the model on T1, where $|T1| = 50$ and then apply clustering to T2 which has the size that is changing from 5 elements to $|T1|*P$. We compare final labels produced by the algorithm to the target labels  and find accuracy as $acc = num_{correct}/num_{all}$, where $num_{correct}$ is the number of points with the correct label, and $num_{all}$ -- the total number of points in T2.\\
To eliminate deviations related to randomized initialization of Gaussians, we perform 10 independent isolated runs of the RPC-SSL model clustering against T1 and T2 for each fixed size of T2 from $N$ and then produce the averaged accuracy of all 10 runs.\\  
As we previously set maximal proportion T2:T1 to $P = 3$, therefore the length of $N$ is set as $|N| = 30$, which corresponds to 600 independent RPC-SSL runs for datasets G-1 and G-2.\\
The final accuracy vector containing 30 averaged accuracies for each G-1 and G-2 is plotted against the proportion: $|T2|/|T1|$. See Fig.\ 5.3:
\begin{figure}[H]
\centering
\includegraphics[width=1.05\textwidth]{./new_t1t2_new.png}
\caption[Figure X]{Results of two iterations of T1-T2 experiment. Blue points correspond to the G-1, red -- to G-2 data}
\label{fig:window}
\end{figure}
The resulting average accuracy per dataset type: $$acc_{G1} = 0.9944, \ \
acc_{G2} = 0.9702$$
As we can see, the model is capable of being trained on the dataset T1 and correctly fit its complexity using T2 without losing its clustering properties. There is no empirical difference between clustering accuracy for relatively small and large T2 comparing to the size of T1. This property of the model can be explained by its prototype creation procedure. With more points added to T2, RPC-SSL simply creates more prototypes in order to better fit the data. 
\\
As the Fig.\ 5.3 demonstrates more complex data G-2 is also not leading to significantly worse results with small-sized T2 (1-10).\\
Another observation can be made from the general trends of both accuracy curves in their latter parts -- corresponding to T2 two or three times larger than T1. Accuracies reported by runs on datasets do not show trends on improvements in accuracy connected to increasing the size of T2. Therefore, the logical conclusion which can be made from [2] is that T2 must be larger than T1 in order to make training successful - is not satisfied.\\ 
Now we can state that SSL-RPC model can be trained and applied to T2 of any proportion or size -- from 1 to 1 million points.
\subsection{SSL-RPC performance comparing to other models}
We compare SSL-RPC performance to other semi-supervised and unsupervised models. The synthetic data was generated using the same technique as in the T1-T2 experiment. We run the model on 25 different sizes of the labeled data T1 ranging from 2 to 50 points while the size of the unlabeled data T2 remains the same. The dataset is generated identically to the \textbf{G-2} case of the T1-T2 experiment -- as two Gaussian blobs with centers (2,2) and (0,0)\\
We chose a number of known models that are being used in supervised and unsupervised learning to compare it to SSL-RPC. The selection was performed to gather models which implement as different approaches to learning as possible. 
\begin{itemize}
\item Semi-supervised label propagation [44] with RBF kernel (denoted as LP-rbf), where the model parameters was selected accordingly to [44,46] where the kernel function is $$ \exp(-\gamma|x-y|^2), \ \gamma = 1 $$
\item Semi-supervised label propagation with KNN kernel, where K = 7 denoted as LP-KNN7. This type of the model was selected as it represents a state-of-art graph-based method with different types of kernel function [59].
\item Semi-supervised label spreading [45] with RBF function (denoted as LS-rbf). The kernel function and $\gamma$ parameter are the same as in the LP case. 
\item Semi-supervised label spreading with KNN-kernel, where K = 7 denoted as LS-KNN7.
\item Supervised Support Vector Machine [47] model with RBF kernel. The error penalty parameter $C = 1$, RBF gamma-coefficient was set to $\gamma = 1$/num\_features. Known labels were fed to SVM model as input alongside with the data, unknown labels were predicted by the model.
\item Unsupervised K-means [48]. The initialization strategy "k-means++" [49] was selected. Note: since KMEANS demonstrated significantly worse results than other models, we decided to limit X-axis representing the model accuracy of Fig.\ 5.4 to 0.65 in order to improve a readability of other models' visualizations.
\item Transductive SVM [61] based on the open-source implementation [116] of the TSVM parser. The model kernel is selected as "rbf", the error penalty parameter $C = 1$, the kernel coefficient parameter $\gamma=0.5$.
\item Self-learning wrapper based on the standard SVM-rbf model from the scikit-learn package [117] with the error penalty parameter $C = 1$, RBF gamma-coefficient was set to $\gamma = 1$/num\_features.
\end{itemize}
In the resulting experiment, we found that SSL-RPC outperformed other models in 30 cases of 30 with an average accuracy $acc_{SSL-RPC} = 0.97$. The resulting accuracies for selected pairs $(|T1|,|T2|)$ can be found in the Table 1. The best resulting model is highlighted with a bold font, the second best -- cursive.
\begin{figure}
\includegraphics[width=1.0\textwidth,left]{./t1t2_new_full.png}
\caption[Figure X.]{Averaged accuracies for SSL-RPC and other models on different T2/T1 size proportions. SSL-RPC (solid blue line) outperforms other models}
\label{fig:window}
\end{figure}
\bgroup
\def\arraystretch{1.5}%
\begin{table}
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline \backslashbox{Model name}{T1 size} & 2 & 10 & 20 & 30 & 40 & 50 \\ 
\hline SSL-RPC & \textbf{1.0} & \textbf{1.0} & \textbf{1.0} & \textbf{1.0} & \textbf{0.975} & \textbf{1.0} \\ 
\hline LP-rbf & 0.885 & 0.85 & 0.905 & \textit{0.885} & 0.91 & \textit{0.925} \\ 
\hline LS-rbf & 0.87 & 0.845 & 0.905 & 0.855 & 0.875 & 0.905 \\ 
\hline LP-KNN-7 & 0.725 & 0.85 & \textit{0.925} & 0.835 & 0.905 & 0.915 \\ 
\hline LS-KNN-7 & 0.565 & 0.865 & 0.915 & 0.86 & 0.89 & 0.895 \\ 
\hline SVM-rbf & 0.885 & 0.845 & 0.91 & 0.86 & \textit{0.92} & 0.91 \\ 
\hline KMEANS & 0.565 & 0.185 & 0.48 & 0.87 & 0.46 & 0.485 \\ 
\hline TSVM & \textit{0.895} & \textit{0.87} & 0.89 & 0.7 & 0.82 & 0.875 \\
\hline Self-learning & 0.885 & 0.855 & 0.905 & 0.88 & 0.915 & 0.915 \\
\hline 
\end{tabular}
\caption{Synthetic data results shows SSL-RPC outperforming other models in the case of simple random blob data. The other model demonstrate close results for most of the cases.}
\end{table}
\egroup
\subsection{Time constraints}
As was stated previously, the data represented in the form of dissimilarity creates challenges for storing dissimilarity matrices of size $N \times N$, where N is the initial data size. In this experiment, we aim at finding the limits for sizes of datasets T1 and T2 with SSL-RPC.\\
Since we are interested in relative growths of run time, we initialize the dataset similarly to T1-T2 experiment setting -- as two Gaussian blobs with centers (3,3) and (0,0). It will allow to decrease the conformal prediction complexity and allow to estimate time spent on calculations involving operations on pair-wise distances matrices. The initial T1 size is set to 50 points, $|T2| = {25, 50, 75, 100, 125, 150, 200, 250, 300, 500, 1000}$ points. We run the model against each pair $(|T1|, |T2|)$ only once.\\
\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{./time.png}
\caption[Figure X]{Execution time for different sizes of T2. The time growth has an exponential form }
\label{fig:window}
\end{figure}
The results demonstrate that run times for $|T2| \leq 250$ are relatively small. For $|T2| = {300, 500}$ we see significant increase of execution time -- up to 50-100 times comparing to the previous size group. Finally, the runtime explodes with 750 points in T2 taking approximately 9 hours to complete the task.
\subsection{Comparing initialization techniques}
We created two Gaussian clusters with centers in [2,2] and [0,0], with standard deviation $\sigma=1$, the same technique that we used in previous examples. The labeled dataset size $|T1| = 30$, unlabeled $|T2|=150$ points. We compared two initialization types: 
\begin{itemize}
\item random initialization of two prototypes -- random float numbers were put to their prototype vectors $\gamma_1, \gamma_2$, all summing up to 1.
\item dataset initialization -- two prototypes were assigned to a randomly labeled point in each class. 
\end{itemize}
The experiment was performed 100 times for each initialization type and the resulting execution time was averaged. Results reported in the Table 5.2.
\bgroup
\def\arraystretch{1.5}%
\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|}
\hline  & Random & Dataset \\ 
\hline Execution time(sec) & 2.529 & \textbf{1.316} \\ 
\hline 
\end{tabular} 
\caption{Training speed depending on the prototype initialization type}
\end{table}
\egroup
The dataset initialization was 1.92 times faster comparing to the random initialization.

\subsection{Regularized SSL-RPC experiment}
One of our contributions to the SSL-RPC model is the proposal to use a regularization coefficient $\frac{1}{\lambda}$ on the Stochastic gradient descent update steps for prototype vectors. During the experiments we noticed an instability in the SSL-RPC performance related to the wide range of a resulting accuracies between two runs on random Gaussian datasets. From the plots, we spotted that in various cases prototypes were pushed or pulled too far away from actual data in wrong locations and the model diverged. We plotted the exact updated prototype positions and realized that the model was performing too large steps when updating prototypes. We decided to regularize update rules with $\frac{1}{\lambda}$ and achieved a stabilized model performance.\\
To demonstrate the described behavior of the model of the non-regularized SSL-RPC we run the $\lambda$-experiment. We varied the value of $\lambda$ from 1 (the non-regularized case) to 1/2000. For each particular $\lambda$ value, 30 runs were completed on the dataset \textbf{G-2} of two random Gaussians with centers (2,2) and (0,0). The averaged run accuracies and standard deviations were calculated and plotted for $1 \leq \lambda \leq 500$. The results are represented in the Table 5.3 and Fig.\ 5.6. Not included results for $600 \leq \lambda \leq 2000$ follow the general trend with a stable average accuracy $acc_{600 \leq \lambda \leq 2000} = 0.9603$ and standard deviation $std_{600 \leq \lambda \leq 2000}=0.0746$.\\
\begin{figure}[h]
\centering
\captionsetup{justification=centering}
\includegraphics[width=1\textwidth,center]{./lambda-error.png}
\caption[Figure X]{$\lambda$-experiment. The red vertical bar over each point corresponds to the standard deviation}
\end{figure}
As it can be seen on the Fig.\ 5.6, the standard deviation of performed runs tends to decrease with increasing $\lambda$ in the regularization coefficient $\frac{1}{\lambda}$.\\
For values $1 \leq \lambda \leq 250$, the averaged accuracies are generally worse than for larger values of $\lambda$, and the standard deviation is located in the range $[0,12,0.27]$. While for larger $\lambda$-s, the average deviation is being in limits $[0.03,0.08]$. This experiment demonstrates the efficiency of the introduced regularization criteria $\frac{{1}}{\lambda}$. At the same time, its important to tune the overall number of the gradient descent steps performed by the SSL-RPC model in one iteration. It has to be increased with increasing the value of $\lambda$. The optimal configuration depends on the requirements to performance/accuracy trade-off in each particular task.
\begin{table}[h]
\centering
\begin{tabular}{|c|c|c|}
\hline $\lambda$ & Accuracy & Standard deviation \\ 
\hline 1 & 0.835 & 0.233 \\ 
\hline 10 & 0.815 & 0.275 \\ 
\hline 20 & 0.842 & 0.205 \\ 
\hline 30 & 0.832 & 0.194 \\ 
\hline 40 & 0.916 & 0.179 \\ 
\hline 50 & 0.864 & 0.204 \\ 
\hline 60 & 0.927 & 0.128 \\ 
\hline 70 & 0.877 & 0.162 \\ 
\hline 80 & 0.877 & 0.164 \\ 
\hline 90 & 0.855 & 0.157 \\ 
\hline 100 & 0.885 & 0.180 \\ 
\hline 150 & 0.931 & 0.158 \\ 
\hline 200 & 0.943 & 0.118 \\ 
\hline 250 & 0.948 & 0.079 \\ 
\hline 200 & 0.941 & 0.099 \\ 
\hline 350 & 0.972 & 0.066 \\ 
\hline 400 & 0.940 & 0.089 \\ 
\hline 450 & 0.970 & 0.073 \\ 
\hline 500 & 0.948 & 0.083 \\ 
\hline 
\end{tabular}
\caption{SSL-RPC results with varying $\lambda$. The larger $\lambda$ is, the better results the model demonstrates, increasing the accuracy and decreasing the standard deviation of classification results} 
\centering
\end{table}
\newpage
\subsection{Model mechanics explained}
As the part of experiments on the toy data, we aim at demonstrating the model's principle of work on the practical example and also verifying that prototype learning is occurred as predicted.\\
In order to construct T1 we set three random Gaussian data blobs with centers in (-1,-1), (0,0) and (1,1) carrying different labels, 50 points in each. T2 is represented by 30 random points created in the same way -- 10 for each class.\\
On the first iteration the initial prototypes are created -- we mark them with small transparent circles on the plot. The larger marker size corresponds to the latter iteration while initial prototypes are the smallest. Prototype positions are defined by points in T2 which formed the set Beta containing data with low confidence/credibility of belonging to one or another cluster (Fig.\ 5.7).
\begin{figure}[h]
\centering
\includegraphics[width=1\textwidth]{./3clust_protos.png}
\caption[Figure X]{Results of four iterations of RPC. The prototype creation procedure takes place 4 times for purple and blue cluster and twice for the red cluster}
\label{fig:window}
\end{figure}
On the next plot (Fig.\ 5.8), with a different initialization of the data, we also marked points of T2 as stars and prototype positions as circles. Under the main data layer we built a simple clustermap -- the cell grid color reflects the possible label assignment according to current prototype positions. We have to note that it does not mean that any T2 point located in one of a colored clusters would receive the same label. On the conformal prediction step, this assignment would shift borders of each cluster.\\
In general, this visualization provides us an insight on the projected model behavior and predicted cluster borders.\\
Remembering the basic principle of GLVQ [1] stating that prototypes positions are being changed with pushing or pulling powers making them better fit the data. From the position of starred points we can observe this influence -- in the bottom left corner blue points from T1 and T2 are interfering with the pink cluster. On the plot, large pink circles show that new prototypes of this cluster were created at around the center of the area where T2 points belong. Also, the first prototype was created closer to the blue border in order to compensate the influence of blue points and push the class borders away from the center.\\
Another observation that could be made is that the border of the Beta region is highly related to T2 points -- red starred dots above the red Beta border are placed closer to the cluster center.\\
\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{./finalfinalfinal.png}
\caption[Figure X]{RPC clustermap for 3 Gaussian blobs colored accordingly to the class assignment. Prototype positions marked as transparent circles; T1 points shown as dots; T2 points correspond to stars.}
\label{fig:window}
\end{figure}
Locations of T2 points (starred) changed the cluster borders significantly. We can see that some of pink points appeared to be on the blue side. It happened due to the dense concentration of blue T2 points (starred) next to them. Three blue prototypes were created in a nearly the same region in order to push blue cluster borders away from the starred points.
\section{Experiments with real-word datasets}
We selected three UCI [112] datasets widely used by Semi-Supervised Learning researchers, according to the publication statistics from the UCI website.\\
\begin{itemize}
\item \textbf{Haberman's Survival Dataset} [113]. Consists of 306 samples of the study of breast cancer surgery patients with 3 features available -- an age of a patient, a year of the operation, a number of positive axillary nodes detected. The samples were labeled as "0" if a patient lived more than 5 years after the operation, and "1" in the case if a patient died within 5 years after the surgery.
\item \textbf{Breast Cancer Wisconsin Diagnosis data} [114], often referred to as WDBC dataset. It consists of 569 records corresponding to images of tumors and 30 features composed of the image information (i.e. tumor radius, texture, perimeter etc.) The class labels are "0" (benign tumor) and "1" (malicious tumor). The nature of the dataset allows to research how well models perform on the multidimensional data (30x).
\item \textbf{Ecoli dataset} [115] composed of 336 data records with 8 numerical attributes each. We selected all records that belong to 5 most common classes. 3 classes "omL", "imL", "imS" containing 5, 2 and 2 records correspondingly were left out of the experiment. The remaining classes were encoded with consecutive numbers from  0 to 4. This dataset was selected to demonstrate abilities of models to work with a large amount of attributes in multi-class environment.
\end{itemize}
For each of the dataset we performed 30 independent runs sampling randomly 10 labeled and 100 unlabeled samples. We used this proportion as we aim our results to be comparable to other SSL-related research that in most of the cases used a relatively small labeled datasets, as we concluded from the analyzed literature [25,59].\\
Also, as a continuation of the $\lambda$-experiment, we added the non-regularized SSL ($\lambda=1$) to the model's list.\\
The resulting accuracy for each model is the average accuracy of all 30 runs.\\
The results are represented in Table 5.4:\\
\bgroup
\def\arraystretch{1.5}%
\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|c|}
\hline \backslashbox{Model}{UCI data} & Haberman & WDBC & Ecoli \\ 
\hline SSL-RPC$_{\lambda=1000}$ & \textbf{0.85} & \textbf{0.907} & \textbf{0.805} \\ 
\hline SSL-RPC$_{\lambda=1}$ &\textit{0.805} & 0.866 & 0.61 \\ 
\hline LP-rbf & 0.622 & 0.625 & 0.462 \\ 
\hline LS-rbf & 0.605 & 0.625 &  \textit{0.769} \\ 
\hline LP-knn-7 & 0.647 & 0.715 & 0.641 \\ 
\hline LS-knn-7 & 0.621 & 0.88 & 0.749 \\ 
\hline SVM(supervised) & 0.64 & 0.839 & 0.383 \\ 
\hline KMEANS & 0.49 & 0.479 & 0.222 \\ 
\hline TSVM & 0.57 & 0.543 & 0.62 \\ 
\hline Self-Learning SVM & 0.62 & \textit{0.89} & 0.44 \\ 
\hline 
\end{tabular}
\caption{UCI data results. SSL-RPC with regularization demonstrates a leading performance. Non-regularized SSL-RPC shows above the average results}
\end{table}
\egroup

SSL-RPC clearly outperformed all other methods. In the case of a WDBC dataset containing a large number of features, SSL-RPC showed the accuracy of 0.85 when the closest result from one of the state-of-art methods was 0.647. Good performance was demonstrated by the original supervised and semi-supervised SVM methods, especially when applying self-learning wrapping technique on the WDBC data.\\
Among all the models, only regularized SSL-RPC demonstrated a stable performance without a dependence on the dataset nature. Results of non-regularized SSL-RPC were worse than its regularized alternative, although clearly above the average.\\
Referring to the original results that researchers of the basic SSL-RPC achieved (Table 5.4), we can see that in the SSL-RPC method with our contribution significantly outperforms the original method on the Haberman data. In the case of the WDBC data set, the original method performs slightly better. In our setup, we performed 30 independent runs sampling randomly 10 labeled and 100 unlabeled samples. It is not stated in [2], how exactly the researchers divided the data set and how many samples did they use. Therefore, the comparison of results can give only the initial understanding on the comparison of how the models perform.
\bgroup
\def\arraystretch{1.5}%
\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|}
\hline \backslashbox{Model}{UCI data} & Haberman & WDBC \\ 
\hline SSL-RPC$_{\lambda=1000}$ & \textbf{0.85} & 0.907 \\ 
\hline SSL-RPC$_{\text{original}}$ & 0.73 & \textbf{0.933} \\ 
\hline
\end{tabular}
\caption{SSL-RPC results comparison between the developed model and the original method}
\end{table}
\egroup

\section{Experiments with sensors data}
The sensor data received over the time was mostly containing normal values or such that only slightly deviate from the expectation. We registered only 5 cases of low-confidence anomalies (classes 2,4) and 1 case of a high-confidence anomaly (class 1, temperature).\\
We manually generated some disturbances in the data by heating up the air and increasing humidity. The model successfully reacted on changes and classified them according to the expectation in 10 cases of 10.\\
Basing on 1400 model runs, the average classification time was 3.2 seconds for $|T1|=15$, $|T2|=5$.\\
The outlier detection algorithm reacted in 0.5 seconds after getting results from the model and successfully raised the alarm when the situation required a human intervention.\\
In the nightly runs when $|T1|=15$, $|T2|=200$ the average running time was 300 seconds. 98.2 \% of unlabeled data was classified correctly.
\section{Open-source community contribution}
We implemented RPC classifiers for supervised / semi-supervised learning (incl. Conformal prediction) in Python as separate modules that can be imported as external packages to any of Python programs. We followed the standard Scikit-Learn [117] naming guidelines for model functionality:
\begin{itemize}
\item RPC\_classifier.fit(T1,T2,T1Labels) -- the method fits RPC classifier basing on T2 only. Takes datasets T1 and T2 as inputs and fits the model using supervised labels T1Labels. 
\item RPC\_classifier.predict() -- the method fits RPC classifier to the T2 data basing on the T1 labels
\item RPC\_classifier.score(T2Labels) -- the method evaluates performance of the RPC classifier if T2Labels are given.
\end{itemize}
The implemented SSL-RPC package is available for downloading and sharing at the public repository: https://github.com/doshyt/ssl-rpc-regularized.git
