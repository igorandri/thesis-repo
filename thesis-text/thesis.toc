\contentsline {section}{\numberline {1}Introduction}{4}
\contentsline {subsection}{\numberline {1.1}The project aims and goals}{5}
\contentsline {subsection}{\numberline {1.2}Proposed framework}{7}
\contentsline {subsection}{\numberline {1.3}Structure}{8}
\contentsline {section}{\numberline {2}Background}{9}
\contentsline {subsection}{\numberline {2.1}Overview of Machine Learning}{9}
\contentsline {subsection}{\numberline {2.2}Semi-Supervised learning}{9}
\contentsline {subsubsection}{\numberline {2.2.1}Generative models in SSL}{10}
\contentsline {subsubsection}{\numberline {2.2.2}Low-density separation models in SSL}{11}
\contentsline {subsubsection}{\numberline {2.2.3}Graph-based methods}{12}
\contentsline {subsubsection}{\numberline {2.2.4}Co-training methods}{13}
\contentsline {subsubsection}{\numberline {2.2.5}Self-training methods}{14}
\contentsline {subsubsection}{\numberline {2.2.6}Prototype-based methods}{14}
\contentsline {subsection}{\numberline {2.3}Learning Vector Quantization}{15}
\contentsline {subsection}{\numberline {2.4}Further development of Learning Vector Quantization}{17}
\contentsline {subsubsection}{\numberline {2.4.1}LVQ methods taxonomy}{17}
\contentsline {subsubsection}{\numberline {2.4.2}Project's classifier selection reasoning}{18}
\contentsline {subsubsection}{\numberline {2.4.3}Generalized Learning Vector Quantization}{19}
\contentsline {subsection}{\numberline {2.5}Dissimilarity data}{21}
\contentsline {subsubsection}{\numberline {2.5.1}Overview of distance measures}{22}
\contentsline {subsection}{\numberline {2.6}Relational Learning Vector Quantization}{23}
\contentsline {subsubsection}{\numberline {2.6.1}Dissimilarity data in LVQ}{23}
\contentsline {subsubsection}{\numberline {2.6.2}RGLVQ with dissimilarity data}{24}
\contentsline {subsection}{\numberline {2.7}Semi-supervised prediction with RPC}{27}
\contentsline {subsection}{\numberline {2.8}Clustering quality estimation}{28}
\contentsline {section}{\numberline {3}Data origins and processing}{31}
\contentsline {subsection}{\numberline {3.1}CIVIS project and the Green Campus Initiative}{31}
\contentsline {subsection}{\numberline {3.2}Data retrieving and processing}{32}
\contentsline {section}{\numberline {4}Algorithms and models}{35}
\contentsline {subsection}{\numberline {4.1}Approach overview}{35}
\contentsline {subsection}{\numberline {4.2}Data preprocessing}{37}
\contentsline {subsection}{\numberline {4.3}Model and training}{39}
\contentsline {subsubsection}{\numberline {4.3.1}Classification algorithm: RPC + SSL}{40}
\contentsline {subsubsection}{\numberline {4.3.2}RPC algorithm in details}{41}
\contentsline {subsubsection}{\numberline {4.3.3}New prototype creation algorithm}{43}
\contentsline {subsubsection}{\numberline {4.3.4}Initialization of prototypes}{46}
\contentsline {subsection}{\numberline {4.4}Algorithm computational complexity}{47}
\contentsline {subsection}{\numberline {4.5}Anomaly detection}{47}
\contentsline {subsection}{\numberline {4.6}Storing data and updating it from newest observation}{49}
\contentsline {section}{\numberline {5}Experiments and results}{50}
\contentsline {subsection}{\numberline {5.1}Experiments with synthetic data}{50}
\contentsline {subsubsection}{\numberline {5.1.1}T1/T2 experiment}{50}
\contentsline {subsubsection}{\numberline {5.1.2}SSL-RPC performance comparing to other models}{54}
\contentsline {subsubsection}{\numberline {5.1.3}Time constraints}{55}
\contentsline {subsubsection}{\numberline {5.1.4}Comparing initialization techniques}{57}
\contentsline {subsubsection}{\numberline {5.1.5}Regularized SSL-RPC experiment}{58}
\contentsline {subsubsection}{\numberline {5.1.6}Model mechanics explained}{58}
\contentsline {subsection}{\numberline {5.2}Experiments with real-word datasets}{61}
\contentsline {subsection}{\numberline {5.3}Experiments with sensors data}{63}
\contentsline {subsection}{\numberline {5.4}Open-source community contribution}{63}
\contentsline {section}{\numberline {6}Discussion and conclusions}{65}
\contentsline {subsection}{\numberline {6.1}SSL models and proposed directions}{65}
\contentsline {subsection}{\numberline {6.2}SSL-RPC development proposals}{65}
\contentsline {subsection}{\numberline {6.3}SSL-RPC drawbacks}{66}
\contentsline {subsection}{\numberline {6.4}Latest scientific achievements}{66}
\contentsline {section}{\numberline {7}References}{68}
